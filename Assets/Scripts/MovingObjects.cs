﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class MovingObjects : MonoBehaviour {

	public float moveTime = 0.1f;

	private BoxCollider2D boxCollider;
	private Rigidbody2D rigidBody;
	private LayerMask collisionLayer;
	
	protected virtual void Start () {
		boxCollider = GetComponent<BoxCollider2D>();
		rigidBody = GetComponent<Rigidbody2D>();
		collisionLayer = LayerMask.GetMask("Collision Layer");
	}

	protected virtual void Move<T>(int xDirection, int yDirection){
	
		RaycastHit2D hit;
		bool canMove = CanObjectMove (xDirection, yDirection, out hit);
		Debug.Log (canMove);
		if (canMove) {
			return;
		}
		T hitComponent = hit.transform.GetComponent<T> ();

		if (hitComponent != null) {
			HandleCollision(hitComponent);
		}
	}

	protected bool CanObjectMove(int xDirection, int yDirection, out RaycastHit2D hit){
		Vector2 startPosition = rigidBody.position;
		Vector2 endPosition = startPosition + new Vector2 (xDirection, yDirection);

		boxCollider.enabled = false;
		hit = Physics2D.Linecast(startPosition, endPosition, collisionLayer);
		boxCollider.enabled = true;

		if(hit.transform == null){

			StartCoroutine (SmoothMovementRoutine (endPosition));
			return true;
		}
		return false;
	
	}

	protected IEnumerator SmoothMovementRoutine(Vector2 endPosition){

		float remainingDistanceToEndPosition;

		do{
			remainingDistanceToEndPosition = (rigidBody.position - endPosition).sqrMagnitude;
			Vector2 updatePosition = Vector2.MoveTowards(rigidBody.position, endPosition, 10f * Time.deltaTime);
			rigidBody.MovePosition(updatePosition);
			yield return null;
		}while(remainingDistanceToEndPosition >  float.Epsilon);
	
	}
	protected abstract void HandleCollision<T>(T component);	
}





